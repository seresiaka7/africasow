
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
require('dotenv').config({path: './config/.env'})
const annonceRoutes=require('./routes/annonceRoutes');
const userRoutes=require('./routes/userRoutes');
require('./config/db');
const {checkUser, requireAuth} = require('./middleware/auth.middleware');
const cors = require('cors');

const app = express();


const corsOptions = {
  origin:"http://localhost:3000" ,
  credentials: true,
  'allowedHeaders': ['sessionId', 'Content-Type'],
  'exposedHeaders': ['sessionId'],
  'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
  'preflightContinue': false
}
app.use(cors());
app.use(cors(corsOptions));
app.use(cookieParser());
 //jwt
app.get('*', checkUser);
app.get('/jwtid', requireAuth, (req, res) => {
  res.status(200).send(res.locals.user._id)
});


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.use('/api/annonce',annonceRoutes);
app.use('/api',userRoutes);
// server
app.listen(process.env.PORT, () => {
  console.log(`Listening on port ${process.env.PORT}`)
});