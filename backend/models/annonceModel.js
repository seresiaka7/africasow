const mongoose = require('mongoose');

const AnnonceSchema = new mongoose.Schema(
  {
    annonceId: {
      type: String,
      required: false
        },
    title: {
      type: String,
      trim: true,
      maxlength: 500,
    },
    
    description: {
      type: String,
        },
    picture: {
      type: String,
        },
    
    telephone: {
      type: Number,
      required: true,
       },
  
  category:{
      type:String,
      required:true,
      },
  ville:{
    type:String,
    required:true,
     },
 prix:{
       type:Number,
       required:true,
       },
    
  createdAt:{
      type:Date,
      default:new Date()
  }
  });


const AnnonceModel = mongoose.model("annonce", AnnonceSchema);

module.exports = AnnonceModel;