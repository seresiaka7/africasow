const router = require('express').Router();
const annonceController = require('../controllers/annonceController');
const multer = require("multer");
const upload = multer();

router.get('/', annonceController.readAnnonce);
router.get('/vehicule', annonceController.getVehicule);
router.get('/immobilier', annonceController.getImmobilier);
router.get('/services', annonceController.getServices);
router.get('/jardin', annonceController.getJardin);
router.get('/informatique', annonceController.getInformatique);
router.get('/telephone', annonceController.getTelephone);
router.post('/insert', annonceController.createAnnonce);
router.post('/:id', annonceController.annonceInfo);
router.put('/:id', annonceController.updateAnnonce);
router.delete('/:id', annonceController.deleteAnnonce);

//upload.single("file"),
module.exports = router;