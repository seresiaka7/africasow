const AnnonceModel = require("../models/annonceModel");
const UserModel = require("../models/userModel");
const { uploadErrors } = require("../utils/errorsUtils");
const ObjectID = require("mongoose").Types.ObjectId;
const fs = require("fs");
const { promisify } = require("util");
const pipeline = promisify(require("stream").pipeline);

module.exports.readAnnonce = (req, res) => {
  AnnonceModel.find((err, docs) => {
    if (!err) res.send(docs);
    else console.log("Error to get data : " + err);
  }).sort({ createdAt: -1 });
};

module.exports.createAnnonce = async (req, res) => {
  

  const newAnnonce = new AnnonceModel({
    annonceId: req.body.annonceId,
    description: req.body.description,
    picture: req.body.picture,
    prix: req.body.prix,
    title: req.body.title,
    telephone: req.body.telephone,
    ville: req.body.ville,
    category: req.body.category
  });

  try {
    const annonce = await newAnnonce.save();
    return res.status(201).json(annonce);
  } catch (err) {
    return res.status(400).send(err);
  }
};

module.exports.updateAnnonce = (req, res) => {
  if (!ObjectID.isValid(req.params.id))
    return res.status(400).send("ID unknown : " + req.params.id);

  const updatedRecord = {
    description: req.body.description,
    picture: req.body.picture,
    prix: req.body.prix,
    title: req.body.title,
    telephone: req.body.telephone,
    ville: req.body.ville,
    category: req.body.category
  };

  AnnonceModel.findByIdAndUpdate(
    req.params.id,
    { $set: updatedRecord },
    { new: true },
    (err, docs) => {
      if (!err) res.send(docs);
      else console.log("Update error : " + err);
    }
  );
};

module.exports.deleteAnnonce = (req, res) => {
  if (!ObjectID.isValid(req.params.id))
    return res.status(400).send("ID unknown : " + req.params.id);

  AnnonceModel.findByIdAndRemove(req.params.id, (err, docs) => {
    if (!err) res.send(docs);
    else console.log("Delete error : " + err);
  });
};

module.exports.annonceInfo = (req, res) => {
  if (!ObjectID.isValid(req.params.id))
    return res.status(400).send("ID unknown : " + req.params.id);

    AnnonceModel.findById(req.params.id, (err, docs) => {
    if (!err) res.send(docs);
    else console.log("ID unknown : " + err);
  })//.select("-title");
};

module.exports.getVehicule = async (req, res) => {
  const users = await AnnonceModel.find({category:"vehicule"});
  res.status(200).json(users);
};
module.exports.getInformatique = async (req, res) => {
  const users = await AnnonceModel.find({category:"informatique"});
  res.status(200).json(users);
};
module.exports.getServices= async (req, res) => {
  const users = await AnnonceModel.find({category:"services"});
  res.status(200).json(users);
};
module.exports.getImmobilier = async (req, res) => {
  const users = await AnnonceModel.find({category:"immobilier"});
  res.status(200).json(users);
};

module.exports.getJardin = async (req, res) => {
  const users = await AnnonceModel.find({category:"jardin"});
  res.status(200).json(users);
};
module.exports.getTelephone= async (req, res) => {
  const users = await AnnonceModel.find({category:"telephone"});
  res.status(200).json(users);
};



